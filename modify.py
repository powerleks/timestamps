import json
import split_text

def remove_dust(text):
    while text[0].isspace():
        text = text[1:]
    count = 0
    i = 0
    while i < len(text):
        if text[i].isspace():
            if count > 0:
                while i < len(text) and text[i].isspace():
                    text = text[:i] + text[i+1:]
                count = 0
            else:
                count += 1
        else:
            count = 0
        i += 1
    while text[-1].isspace():
        text = text[:-2]
    return text

def rubbish_rsport(str):
    begin = 17;
    end = str.find('/', begin)
    return str[begin:end:]

def rubbish_sovsport(text, date):
    if text == "" or date == "":
        return 1
    if text.find("читайте также") != -1:
        return 1
    if date.find("-//") != -1:
        return 1
    return 0

with open('rsport.json') as f:
    articles = json.load(f)

for article in articles:
    article['text'] = split_text.split(article['text'])

with open('rsport_splitted.json', 'w') as ff:
    json.dump(articles, ff, sort_keys = True, 
              indent = 4, ensure_ascii=False)

with open('championat.json') as f:
    articles = json.load(f)

for article in articles:
    article['text'] = split_text.split(article['text'])

with open('championat_splitted.json', 'w') as ff:
    json.dump(articles, ff, sort_keys = True, 
              indent = 4, ensure_ascii=False)

with open('sovsport.json') as f:
    articles = json.load(f)

for article in articles:
    article['text'] = split_text.split(article['text'])

with open('sovsport_splitted.json', 'w') as ff:
    json.dump(articles, ff, sort_keys = True, 
              indent = 4, ensure_ascii=False)