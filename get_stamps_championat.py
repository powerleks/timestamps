import json
import re

with open('championat_splitted.json') as f:
    articles = json.load(f)

def find_stamps(text):
    template = [r'([Вв] (\d\d\d\d) году)', r'([Вв] (\d\d\d\d)-м году)', r'([Вв] (\d\d\d\d)-м)', r'([Кк] (\d\d\d\d) году)', r'([Сс] (\d\d\d\d) года)', r'([Зз]а (\d\d\d\d) год)']
    ans = []

    for i in template:
        ans.extend(re.findall(i, text))

    for i in ans:
        stamp = {}
        stamp['text'] = i[0]
        stamp['normalized'] = i[1] + '.xx.xx'
        stamp['sentence'] = sentence[0]
        article['TimeStamps'].append(stamp)

    template = [r'((летом) (\d{4}) года)', r'((весной) (\d{4}) года)', r'((осенью) (\d{4}) года)', r'((зимой) (\d{4}) года)']
    ans = []

    for i in template:
        ans.extend(re.findall(i, text))

    sub = {'летом': '.06-08.xx', 'весной': '.03-05.xx', 'зимой': '.01-02.xx', 'осенью': '.09-11.xx'}

    for i in ans:
        stamp = {}
        stamp['text'] = i[0]
        stamp['normalized'] = i[2] + sub[i[1]]
        stamp['sentence'] = sentence[0]
        article['TimeStamps'].append(stamp)

    template = [r'((\d{2}) (январ\w*|феврал\w*|март\w*|ма\w*|июн\w*|июл\w*|август\w*|сентябр\w*|октябр\w*|ноябр\w*|декабр\w*) (\d{4}) года)']
    ans = []

    for i in template:
        ans.extend(re.findall(i, text))

    sub = {'янв': '.01.', 'фев': '.02.', 'март': '.03.', 'апр': '.04.', 'ма': '.05.', 'июн': '.06.', 'июл': '.07.', 'авг': '.08.', 'сент': '.09.', 'окт': '.10.', 'ноя': '.11.', 'дек': '.12.'}
    
    for i in ans:
        stamp = {}
        stamp['text'] = i[0]
        for key, val in sub.items():
            if i[2].find(key) != -1:
                if i[2][i[2].find(key) + 2] != 'р':
                    stamp['normalized'] = i[3] + val + i[1]
                else:
                    stamp['normalized'] = i[3] + '.03.' + i[1]
        stamp['sentence'] = sentence[0]
        article['TimeStamps'].append(stamp)

    template = [r'([Вв] (\w+) (\d\d\d\d) года)']
    ans = []

    for i in template:
        ans.extend(re.findall(i, text))

    for i in ans:
        stamp = {}
        stamp['text'] = i[0]
        for key, val in sub.items():
            if i[1].find(key) != -1:
                if i[1][i[1].find(key) + 2] != 'р':
                    stamp['normalized'] = i[2] + val + 'xx'
                else:
                    stamp['normalized'] = i[2] + '.03.' + 'xx'
        stamp['sentence'] = sentence[0]
        article['TimeStamps'].append(stamp)

    template = [r'([Сс] (\d{4}) п{1}о{1} (\d{4}))', r'((\d{4})\-(\d{4}))', r'((\d{4})\-(\d{4}))']
    ans = []

    for i in template:
        ans.extend(re.findall(i, text))

    for i in ans:
        stamp = {}
        stamp['text'] = i[0]
        stamp['normalized'] = i[1] + '-' + i[2] + '.xx.xx'
        stamp['sentence'] = sentence[0]
        article['TimeStamps'].append(stamp)

    template = [r'([Сс] (\d{2}) п{1}о{1} (\d{2}) (\w+))']
    ans = []

    for i in template:
        ans.extend(re.findall(i, text))

    for i in ans:
        stamp = {}
        stamp['text'] = i[0]
        for key, val in sub.items():
            if i[3].find(key) != -1:
                if i[3][i[3].find(key) + 2] != 'р':
                    stamp['normalized'] = 'xx' + val + i[1] + '-' + i[2]
                else:
                    stamp['normalized'] = 'xx' + '.03.' + i[1] + '-' + i[2]
        stamp['sentence'] = sentence[0]
        article['TimeStamps'].append(stamp)

    template = [r'((\w+) (\d{2}) (\w+))']
    ans = []

    for i in template:
        ans.extend(re.findall(i, text))

    for i in ans:
        stamp = {}
        stamp['text'] = i[0]
        if i[1] == 'по':
            continue
        
        is_data_here = False
        for key, val in sub.items():
            if i[3].find(key) != -1:
                is_data_here = True
                if len(i[3]) < i[3].find(key) + 3:
                    is_data_here = False
                    continue
                if i[3][i[3].find(key) + 2] != 'р':
                    stamp['normalized'] = 'xx' + val + i[2]
                else:
                    stamp['normalized'] = 'xx' + '.03.' + i[2]

        if not is_data_here:
            continue
        stamp['sentence'] = sentence[0]
        article['TimeStamps'].append(stamp)

    template = [r'((\w+) (\d{4}) (\w+))']
    ans = []

    for i in template:
        ans.extend(re.findall(i, text))

    banned_pref = ['в', 'к', 'с', 'за', 'летом', 'весной', 'зимой', 'осенью', 'по', 'квартал', 'квартале']
    banned_months = ['январ', 'феврал', 'март', 'апрел', 'май', 'мае', 'июн', 'июл', 'август', 'сентябр', 'октябр', 'нояб', 'декабр'] 

    for i in ans:
        stamp = {}
        stamp['text'] = i[0]
        cond = False
        for j in banned_pref:
            if i[1].lower() == j:
                cond = True
                break
        for j in banned_months:
            if i[1].find(j) != -1:
                cond = True
                break
        if cond:
            continue
        stamp['normalized'] = i[2] + '.xx.xx'
        stamp['sentence'] = sentence[0]
        article['TimeStamps'].append(stamp)

    template = [r'((\w+) кварт\w+ (\d{4}) года)']
    ans = []

    for i in template:
        ans.extend(re.findall(i, text))

    sub = {'перв': '.01-03.xx', 'втор': '.04-06.xx', 'трет': '.07-09.xx', 'четв': '.10-12.xx'}

    for i in ans:
        stamp = {}
        stamp['text'] = i[0]
        for key, val in sub.items():
            if i[1].find(key) != -1:
                stamp['normalized'] = i[2] + val
        stamp['sentence'] = sentence[0]
        article['TimeStamps'].append(stamp)

for article in articles:
    for sentence in article['text'].items():
        find_stamps(sentence[1])

with open('championat_with_timestamps.json', 'w') as ff:
    json.dump(articles, ff, sort_keys = True, 
              indent = 4, ensure_ascii=False)