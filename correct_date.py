import json

with open('rsport_splitted.json') as f:
    articles = json.load(f)

for article in articles:
    date = article['date']
    new_date = date[:4] + '.' + date[4:6] + '.' + date[6:]
    print(new_date)
    article['date'] = new_date

with open('rsport_splitted.json', 'w') as ff:
    json.dump(articles, ff, sort_keys = True, 
              indent = 4, ensure_ascii=False)


with open('sovsport_splitted.json') as f:
    articles = json.load(f)

for article in articles:
    date = article['date']
    new_date = date[:4] + '.' + date[5:7] + '.' + date[8:10]
    article['date'] = new_date

with open('sovsport_splitted.json', 'w') as ff:
    json.dump(articles, ff, sort_keys = True, 
              indent = 4, ensure_ascii=False)


with open('championat_splitted.json') as f:
    articles = json.load(f)

sub = {'января': '.01.', 'февраля': '.02.', 'марта': '.03.', 'апреля': '.04.', 'мая': '.05.', 'июня': '.06.',
       'июля': '.07.', 'августа': '.08.', 'сентября': '.09.', 'октября': '.10.', 'ноября': '.11.', 'декабря': '.12.'}

for article in articles:
    date = article['date'].split(' ')
    new_date = date[2][:4] + sub[date[1]] + date[0]    
    article['date'] = new_date

with open('championat_splitted.json', 'w') as ff:
    json.dump(articles, ff, sort_keys = True, 
              indent = 4, ensure_ascii=False)