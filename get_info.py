def date_championat(str):
    pos = str.find('article__head__date')

    while str[pos] != '>':
        pos += 1
    begin = pos + 1

    while str[pos] != '<':
        pos += 1

    end = pos
    return str[begin:end:]


def date_rsport(str):
    end = str.rfind('/')    
    begin = str.rfind('/', 0, end) + 1
    return str[begin:end:]


def date_sovsport(str):
    pos = str.find('datetime')

    while str[pos] != '"':
        pos += 1

    pos += 1
    begin = pos

    while str[pos] != '"':
        pos += 1

    end = pos
    return str[begin:end:]