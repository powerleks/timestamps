stops = ['.', '?', '!']

def split(text):
    begin = 0
    count = 1
    ans = []
    d = dict()
    for i in range(len(text)):
        if (stops.count(text[i]) != 0 or text[i] == '…') and \
           i + 1 < len(text) and text[i+1] == ' ' and \
           ((i + 2 < len(text)) and (text[i+2].isupper() or \
           text[i+2] == '-' or text[i+2] == '«' or \
           text[i+2] == '–' or text[i+2].isdigit())):
               if text[begin:i].isdigit():
                   continue
               d[count] = text[begin:i+1]
               count += 1
               begin = i + 2
    d[count] = text[begin:len(text)]
    return d