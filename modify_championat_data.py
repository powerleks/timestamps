import get_info
import json
from bs4 import BeautifulSoup
from urllib.request import urlopen

with open('championat.json') as f:
    articles = json.load(f)

for article in articles:
    url = article['url']
    html_doc = urlopen(url).read()
    soup = BeautifulSoup(html_doc)
    article['text'] = soup.find('div', 'text-decor article__contain')
    if article['text'] == None:
        article['text'] = soup.find('div', 'text-decor _reachbanner_ article__contain')
    if article['text'] == None:
        articles.remove(article)
    article['text'] = str(article['text'])


with open('championat_tmp.json', 'w') as ff:
    json.dump(articles, ff, sort_keys = True, 
              indent = 4, ensure_ascii=False)
