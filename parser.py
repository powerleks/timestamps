import get_info
import json
import modify
import newspaper
from newspaper import Article

championat_paper = newspaper.build(
        'http://www.championat.com/', language='ru', 
        memoize_articles=False)


rsport_paper = newspaper.build(
        'http://rsport.ru/', language='ru', 
        memoize_articles=False)


sovsport_paper = newspaper.build(
        'http://www.sovsport.ru/news/', language='ru', 
        memoize_articles=False)


articles = []
it = 0


for article in championat_paper.articles:
    article.download()
    if article.is_downloaded:
        article.parse()
        d = {}
        d['text'] = article.text
        if d['text'] == "":
            continue  
        d['url'] = article.url
        d['date'] = get_info.date_championat(article.html) 
        if d['date'] == "":
            continue  
        d['TimeStamps'] = []
        articles.append(d)
        it += 1
        if (it == 350):
            break


for article in rsport_paper.articles:
    article.download()
    if article.is_downloaded:
        article.parse()
        d = {}
        d['text'] = article.text
        if d['text'] == "":
            continue
        d['url'] = article.url
        rubbish = modify.rubbish_rsport(article.url);
        if (rubbish == "stat"
            or rubbish == "photo"
            or rubbish == "ss_photo"
            or rubbish == "sportguide"):
                continue
        d['date'] = get_info.date_rsport(article.url)
        if d['date'] == "":
            continue
        d['TimeStamps'] = []
        articles.append(d)

for article in sovsport_paper.articles:
    article.download()
    if article.is_downloaded:
        article.parse()
        d = {}
        d['text'] = article.text
        d['url'] = article.url    
        d['date'] = get_info.date_sovsport(article.html)
        if modify.rubbish_sovsport(d['text'], d['date']) == 1:
            continue
        d['TimeStamps'] = []
        articles.append(d)
        it += 1
        if it == 400:
            break


with open('articles.json', 'w') as f:
    json.dump(articles, f, sort_keys = True, 
              indent = 4, ensure_ascii=False)
